package at.plaz.megan;

import at.plaz.force_directed_graph.*;
import at.plaz.force_directed_graph.java_fx.*;
import at.plaz.force_directed_graph.persistence.DAO;
import at.plaz.tuples.DefaultDoubleTupleF;
import at.plaz.tuples.DoubleTuple;
import javafx.application.Application;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.GridPane;
import javafx.stage.Screen;
import javafx.stage.Stage;

import java.io.*;
import java.util.*;

/**
 * Created by Georg Plaz.
 */
public class Main extends Application {

    private MEGANPane meganPane;

    public static final void main(String... args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        meganPane = new MEGANPane();
        DoubleTuple dimensions = new DefaultDoubleTupleF(800, 600);
        Rectangle2D primaryScreenBounds = Screen.getPrimary().getVisualBounds();

        Scene scene = new Scene(meganPane,
                Math.min(dimensions.first(), primaryScreenBounds.getWidth()),
                Math.min(dimensions.second(), primaryScreenBounds.getHeight()));
        stage.setTitle(meganPane.title().get());
        meganPane.title().addValueChangedListener((oldVal, newVal) -> stage.setTitle(newVal));
        stage.setScene(scene);
        stage.sizeToScene();
        stage.show();
    }

    @Override
    public void stop() throws Exception {
        super.stop();
        meganPane.stop();
    }
}
