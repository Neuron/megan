package at.plaz.megan;

import java.util.Deque;
import java.util.LinkedList;

/**
 * Created by Georg Plaz.
 */
public class ResultBuilder {
    private Deque<Tuple> tuples = new LinkedList<>();

    public String buildAcronym() {
        StringBuilder builder = new StringBuilder();
        for (Tuple tuple : tuples) {
            builder.append(tuple.getWord().substring(0, tuple.getIndex()));
        }
        return builder.toString();
    }

    public String buildFullName() {
        StringBuilder builder = new StringBuilder();
        for (Tuple tuple : tuples) {
            if (builder.length() > 0) {
                builder.append(" ");
            }
            for (int i = 0; i < tuple.getIndex(); i++) {
                builder.append(Character.toUpperCase(tuple.getWord().charAt(i)));
            }
            for (int i = tuple.getIndex(); i < tuple.getWord().length(); i++) {
                builder.append(tuple.getWord().charAt(i));
            }
        }
        return builder.toString();
    }

    public Result build() {
        return new Result(buildAcronym(), buildFullName());
    }


    public void add(Tuple tuple) {
        tuples.add(tuple);
    }

    public void removeLast() {
        tuples.removeLast();
    }
}
