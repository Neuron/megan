package at.plaz.megan;

/**
 * Created by Georg Plaz.
 */
public class Tuple {
    private String word;
    private int index;

    public Tuple(String word, int index) {
        this.word = word;
        this.index = index;
    }

    public String getWord() {
        return word;
    }

    public int getIndex() {
        return index;
    }

    public void incrementIndex() {
        index++;
    }
}
