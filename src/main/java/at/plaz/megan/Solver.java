package at.plaz.megan;

import at.plaz.force_directed_graph.Graph;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by Georg Plaz.
 */
public class Solver {
//    private List<at.plaz.acronym_generator.Node> nodes = new LinkedList<>();

//    public void add(at.plaz.acronym_generator.Node node) {
//        nodes.add(node);
//    }

//    public Column addColumn() {
//        Column newColumn = new Column();
//        columns.add(newColumn);
//        return newColumn;
//    }

    public Set<Result> solve(Graph<AcronymNode, ?> graph) {
        Set<AcronymNode> nodes = graph.getNodes();
        for (AcronymNode node : nodes) {
            if (node instanceof GoalNode) {
                ((GoalNode) node).text().set("Waiting for results..");
            }
        }
        Set<Result> results = new LinkedHashSet<>();
        Set<Result> unusedResults = new LinkedHashSet<>();
        for (AcronymNode node : nodes) {
            if (node instanceof StartNode) {
                solveGeneral(((StartNode) node).getDictionary(), node, new ResultBuilder(), results, unusedResults);
            }
        }
        for (AcronymNode node : nodes) {
            if (node instanceof GoalNode) {
                ((GoalNode) node).finalizeResults();
            }
        }
        return results;
    }

    public void solveForChildren(Dictionary dictionary, AcronymNode node, ResultBuilder builder, Set<Result> results, Set<Result> unusedResults) {
        for (AcronymNode child : node.getTargets()) {
            solveGeneral(dictionary, child, builder, results, unusedResults);
        }
    }

    public void solveGeneral(Dictionary dictionary, OptionsNode node, ResultBuilder builder, Set<Result> results, Set<Result> unusedResults) {
        for (Option option : node.getOptions()) {
//            if (!node.mandatory().get()) {
//                solveForChildren(dictionary, node, builder, results, unusedResults);
//            }
            String[] words = option.getWords().toArray(new String[option.getWords().size()]);
            int lastWordLoop = 0;
            WORDS_LOOP:
            for (int wordIndex = 0; wordIndex < words.length; wordIndex++) {
                String word = words[wordIndex];
                char[] nodeCharArray = word.toCharArray();
                Tuple currentTuple = new Tuple(word, 0);
                builder.add(currentTuple);
                lastWordLoop++;
                if (node.helper().get() && wordIndex == words.length-1) {
                    solveForChildren(dictionary, node, builder, results, unusedResults);
                }
                for (int i = 0; i < nodeCharArray.length; i++) {
                    char currentChar = nodeCharArray[i];

                    String currentAcronym = builder.buildAcronym() + currentChar;

                    if (dictionary.containsWordStartingWith(currentAcronym)) {
                        currentTuple.incrementIndex();
//                        if (node.isGoalNode()) {
//                            if (dictionary.contains(currentAcronym)){
//                                Result result = builder.build();
//                                if (results.add(result)) {
//                                    unusedResults.add(result);
//                                }
//                            }
//                        } else {
                        if (wordIndex == words.length - 1) {
                            solveForChildren(dictionary, node, builder, results, unusedResults);
                        }
//                        }
                    } else {
                        if (i == 0) {
                            break WORDS_LOOP;
                        }
                        break;
                    }
                }

            }
            for (int i = 0; i < lastWordLoop; i++) {
                builder.removeLast();
            }
        }
    }
    
    public void solveGeneral(Dictionary dictionary, AcronymNode node, ResultBuilder builder, Set<Result> results, Set<Result> unusedResults) {
        if (node instanceof OptionsNode) {
            solveGeneral(dictionary, (OptionsNode) node, builder, results, unusedResults);
        } else {
            if (node instanceof GoalNode) {
                if (dictionary.contains(builder.buildAcronym())) {
                    Result result = builder.build();
                    if (results.add(result)) {
                        unusedResults.add(result);
                    }
                }
                ((GoalNode) node).addResults(unusedResults);
                unusedResults.clear();
            }
            solveForChildren(dictionary, node, builder, results, unusedResults);
        }
    }
}
