package at.plaz.megan;

import at.plaz.force_directed_graph.Graph;
import at.plaz.force_directed_graph.forces.CircularContainerForce;
import at.plaz.force_directed_graph.forces.Location;
import at.plaz.force_directed_graph.java_fx.CircularContainer;
import at.plaz.force_directed_graph.java_fx.GraphFxXMLDAO;
import at.plaz.force_directed_graph.java_fx.GraphPane;
import at.plaz.force_directed_graph.java_fx.UpdateThread;
import at.plaz.force_directed_graph.persistence.DAO;
import at.plaz.force_directed_graph.value.Property;
import at.plaz.force_directed_graph.value.Protected;
import at.plaz.force_directed_graph.value.SimpleProperty;
import at.plaz.force_directed_graph.value.Value;
import at.plaz.tuples.DefaultDoubleTupleF;
import at.plaz.tuples.DoubleTuples;
import javafx.application.Platform;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Georg Plaz.
 */
public class MEGANPane extends GridPane {
    public static final String PROJECT_NAME = "MAGEN - Magnificent Acronym GENerator";
    public static final File RESOURCES = new File("resources");
    public static final File DEFAULT_PROJECT = new File(RESOURCES, "acronym_creator.xml");
    private GraphPane<AcronymNode, CustomLink> graphPane;
    private Solver solver;
    private Graph<AcronymNode, CustomLink> graph;
    private UpdateThread<AcronymNode, CustomLink> updateThread;
    private Property<File> projectFile = new SimpleProperty<>();
    private GraphFxXMLDAO<AcronymNode, CustomLink> dao;
    private Property<String> title = new SimpleProperty<>();
    private Value<String> readOnlyTitle = new Protected<>(title);
    private static double tabWidth;

    public MEGANPane() {
        setStyle("-fx-background-color: #ffaaff");
        solver = new Solver();

        dao = new GraphFxXMLDAO<>(CustomLink::new);
        dao.addNodeAdapter(new OptionsNodeAdapter());
        dao.addNodeAdapter(new BridgeNodeAdapter());
        dao.addNodeAdapter(new GoalNodeAdapter());
        dao.addNodeAdapter(new StartNodeAdapter());

        load(DEFAULT_PROJECT, false);
        graphPane = new GraphPane<>(graph);

        graphPane.addCreateNodeContextMenuItem("Add Bridge Node", BridgeNode::new);
        graphPane.addCreateNodeContextMenuItem("Add Goal Node", GoalNode::new);
        graphPane.addCreateNodeContextMenuItem("Add Start Node", StartNode::new);
        graphPane.addCreateNodeContextMenuItem("Add Node", OptionsNode::new);
        graphPane.enableLinking(CustomLink::new);

        updateThread = new UpdateThread<>(graph);
        updateThread.start();

        MenuBar menuBar = new MenuBar();
        Menu fileMenu = new Menu("File");
        MenuItem saveItem = new MenuItem("Save ...");
        saveItem.setOnAction(action -> save());
        MenuItem saveAsItem = new MenuItem("Save as");
        saveAsItem.setOnAction(action -> saveAs());
        MenuItem loadItem = new MenuItem("Open ...");
        loadItem.setOnAction(action -> load());
        fileMenu.getItems().addAll(saveItem, saveAsItem, loadItem);

        Menu editMenu = new Menu("Edit");
        MenuItem addStartNodeItem = new MenuItem("Add Start Node");
        addStartNodeItem.setOnAction(action -> graph.addNode(new StartNode(DoubleTuples.ZEROS)));
        MenuItem addGoalNodeItem = new MenuItem("Add Goal Node");
        addGoalNodeItem.setOnAction(action -> graph.addNode(new GoalNode(DoubleTuples.ZEROS)));
        MenuItem addNodeItem = new MenuItem("Add Node Node");
        addNodeItem.setOnAction(action -> graph.addNode(new OptionsNode(DoubleTuples.ZEROS)));
        MenuItem addBridgeNodeItem = new MenuItem("Add Bridge Node");
        addBridgeNodeItem.setOnAction(action -> graph.addNode(new BridgeNode(DoubleTuples.ZEROS)));

        CheckMenuItem fixNodesItem = new CheckMenuItem("Fix Nodes");
        fixNodesItem.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                updateThread = new UpdateThread<>(graph);
                updateThread.start();
            } else {
                updateThread.setRunning(false);
                updateThread = null;
            }
        });

        projectFile.addValueChangedListener((oldVal, newVal) -> updateTitle(newVal));
        editMenu.getItems().addAll(addStartNodeItem, addGoalNodeItem, addNodeItem, addBridgeNodeItem);

        Menu solveMenu = new Menu("Solve");

        menuBar.getMenus().addAll(fileMenu, editMenu, solveMenu);
        add(graphPane, 0, 1);
        add(menuBar, 0, 0);
        setVgrow(graphPane, Priority.ALWAYS);
        setHgrow(graphPane, Priority.ALWAYS);

        solveMenu.setOnAction(action -> solve());
        solveMenu.getItems().add(new MenuItem());
        solveMenu.addEventHandler(Menu.ON_SHOWN, event -> solveMenu.hide());
        solveMenu.addEventHandler(Menu.ON_SHOWING, event -> solveMenu.fire());
        updateTitle(null);

//        graphPane.addGraphicsObject(new CircularContainer(DoubleTuples.ZEROS, 800, 0.01));
    }

    private void updateTitle(File newVal) {
        String prefix = newVal != null ? "" : "* ";
        title.set(prefix + PROJECT_NAME);
    }

    public Value<String> title() {
        return readOnlyTitle;
    }

    private void save(File file) {
        try {
            dao.write(graph, file);
        } catch (DAO.WriteException e) {
            e.printStackTrace();
        }
    }

    private void save() {
        if (!projectFile.isNull()) {
            save(projectFile.get());
        } else {
            saveAs();
        }
    }

    private void saveAs() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save project");
//        fileChooser.setInitialDirectory(folder);
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Graph Project Files", "*.xml"));
        File selectedFile = fileChooser.showSaveDialog(graphPane.getScene().getWindow());
        if(selectedFile != null) {
            save(selectedFile);
            setProjectFile(selectedFile);
        }
    }

    private void load() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Choose new project file");
//        fileChooser.setInitialDirectory(folder);
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Graph Project Files", "*.xml"));
        File selectedFile = fileChooser.showOpenDialog(graphPane.getScene().getWindow());
        if(selectedFile != null) {
            load(selectedFile, true);
        }
    }

    private void load(File projectFile, boolean setProjectFile) {
        try {
            if (graph != null) {
                unloadGraph();
                dao.read(projectFile, graph);
            } else {
                graph = dao.read(projectFile);
            }

            if (setProjectFile) {
                setProjectFile(projectFile);
            }
        } catch (DAO.ReadException e) {
            e.printStackTrace();
        }
    }

    private void setProjectFile(File file) {
        this.projectFile.set(file);
    }

    private void unloadGraph() {
        for (AcronymNode node : new LinkedList<>(graph.getNodes())) {
            graph.remove(node);
        }
    }

    private void solve() {
        new Thread() {
            @Override
            public void run() {


                System.out.println("solving..");
                List<Result> resultList = new LinkedList<>(solver.solve(graph));
                Platform.runLater(() -> {
                    resultList.sort(Comparator.comparing(Result::getAcronym));
                    Stage stage = new Stage();
                    stage.setTitle("Solutions");
                    TextArea root = new TextArea(resultList.stream().map(MEGANPane::format).collect(Collectors.joining("\n")));
                    root.setEditable(false);
                    stage.setScene(new Scene(root, 450, 450));
                    stage.show();
                });

                System.out.println("solved!");
                System.out.println("results:");
                for (Result result : resultList) {
                    System.out.println("\t" + result);
                }
                System.out.println("done!");
            }
        }.start();
    }

    public void stop() {
        updateThread.setRunning(false);
        graphPane.stop();
    }

    private static String format(Result result) {
        if (tabWidth < 0) {
            tabWidth = widthOf("\t");
        }
        String acronym = result.getAcronym().toUpperCase();
        double acronymWidth = widthOf(acronym);
        double missingWidth = tabWidth * 2 - acronymWidth;
        StringBuilder tabs = new StringBuilder();
        double tabCount = missingWidth / tabWidth;
        for (int i = 0; i < tabCount; i++) {
            tabs.append("\t");
        }
        return acronym + tabs + result.getFullName();
    }

    private static double widthOf(String string) {
        Text text = new Text(string);
        new Scene(new Group(text));

        text.applyCss();
        return text.getLayoutBounds().getWidth();
    }
}
