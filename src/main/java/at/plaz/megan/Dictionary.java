package at.plaz.megan;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Georg Plaz.
 */
public class Dictionary {
    private static final char[] CHARS = "abcdefghijklmnopqrstuvwxyz".toCharArray();

    public Dictionary() {

    }

    public Dictionary(Collection<String> words) {
        words.forEach(this::add);
    }

    private Node root = new Node();

    public void add(String word) {
        root.add(word);
    }

    public boolean contains(String word){
        return root.containsWord(word);
    }

    public boolean containsWordStartingWith(String prefix) {
        return root.containsWordStartingWith(prefix);
    }

    public int size(){
        return root.size();
    }

    public void remove(String word) {
        root.remove(word);
    }

    private class Node {
        private int size = 0;
        private int depth;
        private char c;
        private Node parent;
        private Node[] children = new Node[CHARS.length];
        private boolean isWord;

        public Node() {
            this('#', 0, null, false);
        }

        public Node(char c, Node parent, boolean isWord) {
            this(c, parent.depth+1, parent, isWord);
        }

        private Node(char c, int depth, Node parent, boolean isWord) {
            this.depth = depth;
            this.c = c;
            this.parent = parent;
            this.isWord = isWord;
        }

        public int size() {
            return size;
        }

        public char getC() {
            return c;
        }

        public boolean add(String word) {
            if (word.length() == depth) {
                if (!isWord) {
                    size++;
                    isWord = true;
                    return true;
                } else {
                    return false;
                }
            } else {
                char c = word.charAt(depth);
                int index = toIndex(c);
                Node childNode = children[index];
                if (childNode == null) {
                    childNode = new Node(c, depth + 1, this, false);
                    children[index] = childNode;
                }
                if (childNode.add(word)) {
                    size++;
                    return true;
                } else {
                    return false;
                }
            }
        }

        public boolean remove(String word) {
            if (word.length() == depth) {
                if (isWord) {
                    size--;
                    if (size < 0) {
                        throw new RuntimeException();
                    }
                    isWord = false;
                    return true;
                } else {
                    return false;
                }
            } else {
                char c = word.charAt(depth);
                int index = toIndex(c);
                Node childNode = children[index];
                if (childNode == null) {
                    return false;
                } else if (childNode.remove(word)) {
                    if (childNode.size == 0) {
                        children[index] = null;
                    }
                    size--;
                    if (size < 0) {
                        throw new RuntimeException();
                    }
                    return true;
                } else {
                    return false;
                }
            }
        }

        public List<String> collect(){
            List<String> collection = new ArrayList<>(size());
            collect(new StringBuilder(), collection);
            return collection;
        }

        private void collect(StringBuilder parentStringBuilder, List<String> collection){
            if (parent != null){
                parentStringBuilder.append(c);
            }
            if (isWord) {
                collection.add(parentStringBuilder.toString());
            }
            for (int i = 0; i < CHARS.length; i++) {
                Node childNode = children[i];
                if (childNode != null) {
                    childNode.collect(parentStringBuilder, collection);
                }
            }
            if (parent != null){
                parentStringBuilder.deleteCharAt(depth-1);
            }
        }

        public boolean containsWord(String word) {
            if (word.length() == depth) {
                return isWord;
            } else {
                char c = word.charAt(depth);
                Node child = children[toIndex(c)];
                return child != null && child.containsWord(word);
            }
        }

        public boolean containsWordStartingWith(String prefix) {
            if (prefix.length() == depth) {
                return true;
            } else {
                char c = prefix.charAt(depth);
                Node child = children[toIndex(c)];
                return child != null && child.containsWordStartingWith(prefix);
            }
        }
    }

    public static int toIndex(char c) {
        return (int) Character.toLowerCase(c) - (int) 'a';
    }

    @Override
    public String toString() {
        return root.collect().toString();
    }
}
