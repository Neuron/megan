package at.plaz.megan;

import at.plaz.force_directed_graph.GenericNodeAdapter;
import at.plaz.force_directed_graph.value.StorableNode;
import at.plaz.tuples.DoubleTuple;

/**
 * Created by Georg Plaz.
 */
public class GoalNodeAdapter extends GenericNodeAdapter<GoalNode> {
    public GoalNodeAdapter() {
        super("goal_node", GoalNode.class, GoalNode::new);
    }

    @Override
    public void addStorables(GoalNode node, StorableNode storables) {
        super.addStorables(node, storables);
        storables.addString("solution", node.text());
    }

    private GoalNode create(DoubleTuple doubles) {
        GoalNode result = new GoalNode(doubles);
        return result;
    }
}
