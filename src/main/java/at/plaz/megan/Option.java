package at.plaz.megan;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Georg Plaz.
 */
public class Option {
    private List<String> words = new LinkedList<>();
    public Option(String content) {
        for (String s : content.split("\\s")) {
            words.add(s.trim());
        }
    }

    public List<String> getWords() {
        return words;
    }

    public String compile() {
        StringBuilder builder = new StringBuilder();
        for (String word : words) {
            if (builder.length() > 0) {
                builder.append(' ');
            }
            builder.append(word);
        }
        return builder.toString();
    }

    @Override
    public String toString() {
        return compile();
    }
}
