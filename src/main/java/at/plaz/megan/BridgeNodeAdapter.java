package at.plaz.megan;

import at.plaz.force_directed_graph.GenericNodeAdapter;
import at.plaz.force_directed_graph.value.StorableNode;

/**
 * Created by Georg Plaz.
 */
public class BridgeNodeAdapter extends GenericNodeAdapter<BridgeNode> {
    public BridgeNodeAdapter() {
        super("bridge_node", BridgeNode.class, BridgeNode::new);
    }

    @Override
    public void addStorables(BridgeNode node, StorableNode storables) {
        super.addStorables(node, storables);
    }
}
