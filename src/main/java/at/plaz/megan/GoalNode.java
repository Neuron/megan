package at.plaz.megan;

import at.plaz.force_directed_graph.Force;
import at.plaz.force_directed_graph.Graph;
import at.plaz.force_directed_graph.Node;
import at.plaz.force_directed_graph.NodeForce;
import at.plaz.force_directed_graph.forces.EquilibriumForce;
import at.plaz.force_directed_graph.java_fx.GraphPane;
import at.plaz.force_directed_graph.java_fx.TextNode;
import at.plaz.force_directed_graph.value.DoubleTupleProperty;
import at.plaz.tuples.DoubleTuple;
import javafx.scene.paint.Color;

import java.util.*;

/**
 * Created by Georg Plaz.
 */
public class GoalNode extends TextNode implements AcronymNode {
    private Set<AcronymNode> targets = new LinkedHashSet<>();
    private Set<AcronymNode> sources = new LinkedHashSet<>();
    private Set<Result> results = new LinkedHashSet<>();

    public GoalNode(DoubleTuple center) {
        super(center);
        graphicsObject().get().maxWidth().set(600.);
        graphicsObject().get().minWidth().set(200.);
        graphicsObject().get().getTextArea().setEditable(false);
        graphicsObject().get().getDragHandle().setFill(Color.valueOf("aaffaa"));
        graphPane().addValueChangedListener((oldValue, newValue) ->
                setUpTargetsSources((GraphPane<AcronymNode, CustomLink>) newValue, sources, targets));
//        ((Region)getTextArea().lookup(".content")).setBackground(new Background(new BackgroundFill(Color.valueOf("aaffaa"), null, null)));
//        getTextArea().setStyle("" +
//                ".text-area .content {\n" +
//                "    -fx-background-color: #000000;\n" +
//                "}");
    }

    @Override
    public Set<AcronymNode> getTargets() {
        return targets;
    }

    @Override
    public Set<AcronymNode> getSources() {
        return sources;
    }

    @Override
    public boolean isGoalNode() {
        return true;
    }

    public void addResults(Set<Result> results) {
        this.results.addAll(results);
    }

    public void finalizeResults() {
        List<String> solutions = new LinkedList<>();
        for (Result result : results) {
            solutions.add(result.toString());
        }
        Collections.sort(solutions);
        StringBuilder builder = new StringBuilder();
        Iterator<String> iterator = solutions.iterator();
        while (true) {
            String line = iterator.next();
            builder.append(line);
            if (!iterator.hasNext()) {
                break;
            } else {
                builder.append('\n');
            }
        }
        text().set(builder.toString());
        results.clear();
    }

//    @Override
//    public Force<?> createDefaultForce(Graph<?, ?> graph) {
//        EquilibriumForce force = graph.createEquilibriumForce(this);
//        force.equilibriumPosition().unbind();
//        force.equilibriumPosition().set(400);
//
//        return force;
//    }

//    @Override
//    public NodeForce getForce(Node other) {
//        if (!getGraph().isLinked(this, other)) {
//            return super.getForce(other);
//        } else {
//            EquilibriumForce<AcronymNode> force = new EquilibriumForce<>(graphPane.getGraph());
//            force.equilibriumPosition().set(500.);
//            force.attractivePower().set(3.);
//            force.repulsivePower().set(3.);
//            force.intensity().set(0.1);
//            return force;//new SimpleRepulsiveForce<>(dimensionalScale);
//        }
//    }
}
