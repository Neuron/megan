package at.plaz.megan;

import at.plaz.force_directed_graph.java_fx.CircularNode;
import at.plaz.force_directed_graph.java_fx.GraphPane;
import at.plaz.tuples.DoubleTuple;
import javafx.scene.paint.Color;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by Georg Plaz.
 */
public class BridgeNode extends CircularNode implements AcronymNode {
    private Set<AcronymNode> targets = new LinkedHashSet<>();
    private Set<AcronymNode> sources = new LinkedHashSet<>();

    public BridgeNode(DoubleTuple center) {
        super(center, 20);
        getCircleGraphics().getCircle().setFill(Color.TRANSPARENT);

        graphPane().addValueChangedListener((oldValue, newValue) ->
                setUpTargetsSources((GraphPane<AcronymNode, CustomLink>) newValue, sources, targets));
    }

    @Override
    public Set<AcronymNode> getTargets() {
        return targets;
    }

    @Override
    public Set<AcronymNode> getSources() {
        return sources;
    }

}
