package at.plaz.megan;

import at.plaz.force_directed_graph.Force;
import at.plaz.force_directed_graph.Graph;
import at.plaz.force_directed_graph.forces.EquilibriumForce;
import at.plaz.force_directed_graph.java_fx.DimensionalAwareEquilibriumForce;
import at.plaz.force_directed_graph.java_fx.GraphPane;
import at.plaz.force_directed_graph.java_fx.TextNode;
import at.plaz.force_directed_graph.java_fx.fx_value.FileDialogBuilder;
import at.plaz.force_directed_graph.java_fx.fx_value.FxProperty;
import at.plaz.tuples.DoubleTuple;
import javafx.scene.control.Button;
import javafx.scene.paint.Color;

import java.io.*;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by Georg Plaz.
 */
public class StartNode extends TextNode implements AcronymNode {
    private Set<AcronymNode> targets = new LinkedHashSet<>();
    private Set<AcronymNode> sources = new LinkedHashSet<>();
    private GraphPane<AcronymNode, CustomLink> graphPane;
    private FxProperty<File> file = new FxProperty<>(new FileDialogBuilder("Choose Word List", null, new File("resources")));
    private Dictionary dictionary = new Dictionary();
    private EquilibriumForce force;

    public StartNode(DoubleTuple center) {
        super(center);
        graphicsObject().get().maxWidth().set(600.);
        graphicsObject().get().minWidth().set(100.);
        graphicsObject().get().getTextArea().setEditable(false);
        graphicsObject().get().getDragHandle().setFill(Color.SADDLEBROWN.brighter());
        if (!file.isNull() && !file.get().exists()) {
            file.set(null);
        }
        Button changeFileButton = new Button("Change Path");
        graphicsObject().get().getBottomContent().getChildren().add(changeFileButton);
        changeFileButton.setOnAction(action -> openDialog());
        file.addValueChangedListener((oldVal, newVal) -> {
            dictionary = new Dictionary();
            if (newVal != null) {
                if (newVal.exists()) {
                    text().set(newVal.getPath());
                    BufferedReader bufferedReader = null;
                    try {
                        bufferedReader = new BufferedReader(new FileReader(newVal));
                        String line;
                        while ((line = bufferedReader.readLine()) != null) {
                            line = line.toLowerCase();
                            for (String s : line.split("[^a-z]")) {
                                if (!s.isEmpty()) {
                                    dictionary.add(s);
                                }
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    if (oldVal.exists()) {
                        file.set(oldVal);
                    } else {
                        file.set(null);
                    }
                }
            }
        });
        graphPane().addValueChangedListener((oldValue, newValue) ->
                setUpTargetsSources((GraphPane<AcronymNode, CustomLink>) newValue, sources, targets));
    }

//    @Override
//    public Force<?> createDefaultForce(Graph<?, ?> graph) {
//        force = new DimensionalAwareEquilibriumForce(this, 1.5);
//        force.attractivePower().set(3);
//        force.repulsivePower().set(3);
//        force.intensity().set(0.1);
//        return force;
//    }

    public void openDialog() {
        file.setUserInput();
    }

    public FxProperty<File> file() {
        return file;
    }

    @Override
    public Set<AcronymNode> getTargets() {
        return targets;
    }

    @Override
    public Set<AcronymNode> getSources() {
        return sources;
    }

    public Dictionary getDictionary() {
        return dictionary;
    }

    @Override
    public boolean isStartNode() {
        return true;
    }
}
