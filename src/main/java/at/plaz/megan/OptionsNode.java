package at.plaz.megan;

import at.plaz.force_directed_graph.Force;
import at.plaz.force_directed_graph.Graph;
import at.plaz.force_directed_graph.java_fx.GraphPane;
import at.plaz.force_directed_graph.java_fx.TextNode;
import at.plaz.force_directed_graph.value.*;
import at.plaz.tuples.DefaultDoubleTupleF;
import at.plaz.tuples.DoubleTuple;
import javafx.scene.control.CheckBox;
import javafx.scene.paint.Color;

import java.util.*;

/**
 * Created by Georg Plaz.
 */
public class OptionsNode extends TextNode implements AcronymNode {
    private List<Option> options = new LinkedList<>();
    private Property<Boolean> helper = new SimpleProperty<>(false);
    private Set<AcronymNode> targets = new LinkedHashSet<>();
    private Set<AcronymNode> sources = new LinkedHashSet<>();
    private boolean listen = true;
    private CheckBox helperCheck = new CheckBox("is helper");

    public OptionsNode(double x, double y) {
        this(new DefaultDoubleTupleF(x, y));
    }

    public OptionsNode(DoubleTuple center) {
        super(center);
        linkBoolean(helperCheck, helper());
        graphicsObject().get().getBottomContent().getChildren().add(helperCheck);
        graphicsObject().get().getDragHandle().setFill(Color.LIGHTGRAY);
        text().addValueChangedListener((oldVal, newVal) -> {
            if (listen) {
                listen = false;
                options.clear();
                for (String line : text().get().split("\n")) {
                    addOption(line);
                }
                listen = true;
            }
        });
        graphPane().addValueChangedListener((oldValue, newValue) ->
                setUpTargetsSources((GraphPane<AcronymNode, CustomLink>) newValue, sources, targets));
    }

//    @Override
//    public Force<?> createDefaultForce(Graph<?, ?> graph) {
//        return super.createDefaultForce(graph);
//    }

    private void linkBoolean(CheckBox checkBox, Property<Boolean> property){
        property.addValueChangedListener((oldVal, newVal) -> {
            if(checkBox.isSelected() != newVal) {
                checkBox.setSelected(newVal);
            }
        });
        checkBox.selectedProperty().addListener((observable, oldVal, newVal) -> {
            if(property.get().booleanValue() != newVal.booleanValue()) {
                property.set(newVal);
            }
        });
    }

    public AcronymNode addOption(Option option) {
        options.add(option);
        if (listen) {
            listen = false;
            if (text().isNull() || text().get().isEmpty()) {
                text().set(option.toString());
            } else {
                text().set(text().get() + "\n" + option.toString());
            }
            listen = true;
        }
        return this;
    }

    public AcronymNode addOptions(Option... options) {
        for (Option option : options) {
            addOption(option);
        }
        return this;
    }

    public AcronymNode addOptions(String... strings) {
        for (String s : strings) {
            addOption(new Option(s));
        }
        return this;
    }

    public AcronymNode addOption(String string) {
        addOption(new Option(string));
        return this;
    }

    public List<Option> getOptions() {
        return options;
    }

    public Property<Boolean> helper() {
        return helper;
    }

    @Override
    public Set<AcronymNode> getTargets() {
        return targets;
    }

    @Override
    public Set<AcronymNode> getSources() {
        return sources;
    }
}
