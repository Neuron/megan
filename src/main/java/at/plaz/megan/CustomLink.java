package at.plaz.megan;


import at.plaz.force_directed_graph.java_fx.GraphPane;
import at.plaz.force_directed_graph.java_fx.LinkFx;
import at.plaz.tuples.DoubleTuple;
import at.plaz.tuples.DoubleTupleF;
import javafx.application.Platform;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.StrokeType;

import static at.plaz.tuples.DoubleTuples.*;

/**
 * Created by Georg Plaz.
 */
public class CustomLink extends LinkFx<AcronymNode> {
    private Circle circle = new Circle(5, Color.WHITE);
    public double secondsPerCycle = 1;
//    private long startMillis;
//    private Path path = new Path();
//    private MoveTo firstMoveTo;
//    private LineTo secondMoveTo;
//    private PathTransition pathTransition;

    public CustomLink(AcronymNode first, AcronymNode second) {
        super(first, second);
        circle.setStroke(Color.BLACK);
        circle.setStrokeType(StrokeType.INSIDE);
        getChildren().add(circle);
    }

    @Override
    public void setGraphPane(GraphPane<AcronymNode, ? extends LinkFx<AcronymNode>> graphPane) {
        super.setGraphPane(graphPane);
        graphPane.addTickListener(this::uiUpdate);
    }

    private void uiUpdate(long timeDelta) {
        DoubleTuple startPoint = getFirst().location().get();
        DoubleTuple endPoint = getSecond().location().get();
        DoubleTuple delta = subtract(endPoint, startPoint);
        double timeDeltaInSecs = (timeDelta/1000.) % secondsPerCycle;
        DoubleTupleF newCenter = add(startPoint, multiply(delta, timeDeltaInSecs));
        Platform.runLater(() -> {
            circle.setCenterX(newCenter.first());
            circle.setCenterY(newCenter.second());
        });
    }

    @Override
    public void update() {
        super.update();
    }

//    private void resetPath() {
//        if (pathTransition != null) {
//            pathTransition.stop();
//        }
//        pathTransition = new PathTransition();
//        pathTransition.setDuration(Duration.millis(1000));
//        pathTransition.setPath(path);
//        pathTransition.setNode(circle);
//        pathTransition.setOrientation(PathTransition.OrientationType.ORTHOGONAL_TO_TANGENT);
//        pathTransition.setCycleCount(Timeline.INDEFINITE);
//        pathTransition.setAutoReverse(false);
//        DoubleTuple startPoint = getFirst().location().get();
//        DoubleTuple endPoint = getSecond().location().get();
//        firstMoveTo = new MoveTo(startPoint.first(), startPoint.second());
//        secondMoveTo = new LineTo(endPoint.first(), endPoint.second());
//        path.getElements().clear();
//        path.getElements().addAll(firstMoveTo, secondMoveTo);
//    }

//    private void updatePath() {
//        Platform.runLater(() -> {
//
//            Duration lastDuration = pathTransition.getCurrentTime();
//            System.out.println(lastDuration);
//            resetPath();
//            pathTransition.jumpTo(lastDuration);
//            pathTransition.play();

//            pathTransition.pause();
//            path.getElements().clear();
//
//            DoubleTuple startPoint = getFirst().location().get();
//            DoubleTuple endPoint = getSecond().location().get();
//            firstMoveTo.setX(startPoint.first());
//            firstMoveTo.setY(startPoint.second());
//            secondMoveTo.setX(endPoint.first());
//            secondMoveTo.setY(endPoint.second());
//            path.getElements().addAll(new MoveTo(circle.getTranslateX(), circle.getTranslateY()), secondMoveTo);
////            pathTransition.play();
//            pathTransition.setCycleCount(1);
//            pathTransition.play();
//            pathTransition.setOnFinished(event -> {
//                pathTransition.setCycleCount(Timeline.INDEFINITE);
//                resetPath();
//            });

//        });
//    }
}
