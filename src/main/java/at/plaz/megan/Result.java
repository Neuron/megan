package at.plaz.megan;

/**
 * Created by Georg Plaz.
 */
public class Result {
    private String acronym;
    private String fullName;

    public Result(String acronym, String fullName) {
        this.acronym = acronym;
        this.fullName = fullName;
    }

    @Override
    public String toString() {
        return acronym.toUpperCase() + "\t" + fullName;
    }

    public String getAcronym() {
        return acronym;
    }

    public String getFullName() {
        return fullName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Result)) return false;

        Result result = (Result) o;

        if (acronym != null ? !acronym.equals(result.acronym) : result.acronym != null) return false;
        return fullName != null ? fullName.equals(result.fullName) : result.fullName == null;

    }

    @Override
    public int hashCode() {
        int result = acronym != null ? acronym.hashCode() : 0;
        result = 31 * result + (fullName != null ? fullName.hashCode() : 0);
        return result;
    }
}
