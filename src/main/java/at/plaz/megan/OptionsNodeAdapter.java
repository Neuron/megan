package at.plaz.megan;

import at.plaz.force_directed_graph.GenericNodeAdapter;
import at.plaz.force_directed_graph.value.StorableNode;

/**
 * Created by Georg Plaz.
 */
public class OptionsNodeAdapter extends GenericNodeAdapter<OptionsNode> {
    public OptionsNodeAdapter() {
        super("acronym_node", OptionsNode.class, OptionsNode::new);
    }

    @Override
    public void addStorables(OptionsNode node, StorableNode storables) {
        super.addStorables(node, storables);
        storables.addString("text", node.text());
        storables.addBoolean("is_helper", node.helper());
    }
}
