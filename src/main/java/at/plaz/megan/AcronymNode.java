package at.plaz.megan;

import at.plaz.force_directed_graph.Draggable;
import at.plaz.force_directed_graph.Link;
import at.plaz.force_directed_graph.Node;
import at.plaz.force_directed_graph.java_fx.GraphPane;
import at.plaz.force_directed_graph.java_fx.NodeFx;

import java.util.Set;

/**
 * Created by Georg Plaz.
 */
public interface AcronymNode extends Node, Draggable, NodeFx {
    Set<AcronymNode> getTargets();

    Set<AcronymNode> getSources();

    default boolean isGoalNode() {
        return getTargets().size() == 0;
    }

    default boolean isStartNode() {
        return getSources().size() == 0;
    }

    default void setUpTargetsSources(GraphPane<AcronymNode, CustomLink> graphPane, Set<AcronymNode> sources, Set<AcronymNode> targets) {
        for (Link<AcronymNode> link : graphPane.getGraph().getLinks(this)) {
            if (link.getFirst() == this) {
                targets.add(link.getSecond());
            }
            if (link.getSecond() == this) {
                sources.add(link.getFirst());
            }
        }

        graphPane.getGraph().addLinkAddedListener(link -> {
            if (link.getFirst() == AcronymNode.this) {
                targets.add(link.getSecond());
                checkTargets();
            } else if (link.getSecond() == AcronymNode.this) {
                sources.add(link.getFirst());
                checkSources();
            }
        });
        graphPane.getGraph().addLinkRemovedListener(link -> {
            if (link.getFirst()==AcronymNode.this) {
                targets.remove(link.getSecond());
                checkTargets();
            } else if (link.getSecond() == AcronymNode.this) {
                sources.remove(link.getFirst());
                checkSources();
            }
        });
    }

    default void checkSources(){}

    default void checkTargets(){}
}
