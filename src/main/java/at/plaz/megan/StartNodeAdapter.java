package at.plaz.megan;

import at.plaz.force_directed_graph.GenericNodeAdapter;
import at.plaz.force_directed_graph.value.StorableNode;

/**
 * Created by Georg Plaz.
 */
public class StartNodeAdapter extends GenericNodeAdapter<StartNode> {
    public StartNodeAdapter() {
        super("start_node", StartNode.class, StartNode::new);
    }

    @Override
    public void addStorables(StartNode node, StorableNode storables) {
        super.addStorables(node, storables);
        storables.addFile("file", node.file());
    }
}
